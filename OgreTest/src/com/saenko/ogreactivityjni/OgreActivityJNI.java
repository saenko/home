package com.saenko.ogreactivityjni;


import android.content.res.AssetManager;
import android.view.Surface;

public class OgreActivityJNI {	
	public native static void create(AssetManager mgr);	
	public native static void destroy();	
	public native static void initWindow(Surface surface);
	public native static void termWindow();
	public native static void renderOneFrame();
}
