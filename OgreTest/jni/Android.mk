LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE    := OgreJNI

OGRE_PATH 		:= /Users/vasya/ogre
OPEN_CV			:= /Users/vasya/Documents/bloodex/Android/OpenCV-2.4.8-android-sdk/sdk/native
OGRE_ANDROID_SDK := /Users/vasya/Documents/bloodex/Android/Ogre_Android_SDK
PATH_OGRE := /Users/vasya/Documents/bloodex/Android/Ogre_Android_SDK/Ogre


$(info $(OGRE_PATH))
$(info $(OPEN_CV))
$(info $(OGRE_ANDROID_SDK))
$(info $(LOCAL_PATH))


#include $(OPEN_CV)jni/OpenCV.mk

LOCAL_LDLIBS	:= -landroid -lc -lm -ldl -llog -lEGL -lGLESv2
LOCAL_LDLIBS	+= -lPlugin_ParticleFXStatic -lPlugin_OctreeSceneManagerStatic 
LOCAL_LDLIBS	+= -lRenderSystem_GLES2Static -lOgreOverlayStatic -lOgreMainStatic
LOCAL_LDLIBS	+= -lzzip -lz -lFreeImage -lfreetype -lOIS  

LOCAL_LDLIBS	+=   $(OGRE_PATH)/AndroidDependencies/lib/armeabi-v7a/libsupc++.a 
LOCAL_LDLIBS	+=   $(OGRE_PATH)/AndroidDependencies/lib/armeabi-v7a/libstdc++.a
LOCAL_STATIC_LIBRARIES := android_native_app_glue cpufeatures

LOCAL_CFLAGS += -fexceptions -frtti -x c++ -D___ANDROID___ -DANDROID -DZZIP_OMIT_CONFIG_H -DUSE_RTSHADER_SYSTEM=1

LOCAL_LDLIBS	+= -L$(OGRE_PATH)/AndroidDependencies/lib/armeabi-v7a/

LOCAL_LDLIBS	+= -L$(OPEN_CV)/jni/include/opencv2
LOCAL_LDLIBS	+= -L$(OPEN_CV)/jni/include/opencv2/core
LOCAL_LDLIBS	+= -L$(OPEN_CV)/jni/include/opencv2/highgui
LOCAL_LDLIBS	+= -L$(OPEN_CV)/libs/armeabi-v7a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_core.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_highgui.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_videostab.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_contrib.a

LOCAL_LDLIBS	+= $(OGRE_PATH)/AndroidDependencies/lib/armeabi-v7a/libfreetype.a
LOCAL_LDLIBS	+= $(OGRE_PATH)/AndroidDependencies/lib/armeabi-v7a/libFreeImage.a
LOCAL_LDLIBS	+= $(OGRE_PATH)/AndroidDependencies/lib/armeabi-v7a/libzzip.a

LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_androidcamera.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_calib3d.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_contrib.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_core.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_features2d.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_flann.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_highgui.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_imgproc.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_java.so
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_legacy.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_ml.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_objdetect.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_ocl.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_photo.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_stitching.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_ts.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_video.a
LOCAL_LDLIBS	+= -l$(OPEN_CV)/libs/armeabi-v7a/libopencv_videostab.a

LOCAL_LDLIBS	+= -L$(PATH_OGRE)/lib/armeabi-v7a
LOCAL_LDLIBS	+= -L$(ANDROID_NDK)/platforms/android-9/arch-arm/usr/lib
LOCAL_LDLIBS	+= -l$(LOCAL_PATH)/../obj/local/armeabi-v7a/libcpufeatures.a

LOCAL_CFLAGS := -DGL_GLEXT_PROTOTYPES=1

LOCAL_CFLAGS += -I$(ANDROID_NDK)/sources/cpufeatures 
LOCAL_CFLAGS += -I$(OGRE_PATH)/Ogre/include/  
LOCAL_CFLAGS += -I$(ANDROID_NDK)/sources/cpufeatures 
LOCAL_CFLAGS += -I$(ANDROID_NDK)/platforms/android-9/arch-arm/usr/include 
LOCAL_CFLAGS += -I$(OGRE_PATH)/Dependencies/include 

LOCAL_CFLAGS += -I$(OPEN_CV)/jni
LOCAL_CFLAGS += -I$(OPEN_CV)/jni/opencv2
LOCAL_CFLAGS += -I$(OPEN_CV)/jni/opencv2/core
LOCAL_CFLAGS += -I$(OPEN_CV)/jni/opencv2/highgui
LOCAL_CFLAGS += -I$(OPEN_CV)/jni/include
LOCAL_CFLAGS += -I$(OPEN_CV)/jni/include/opencv2
LOCAL_CFLAGS += -I$(OPEN_CV)/jni/include/opencv2/core
LOCAL_CFLAGS += -I$(OPEN_CV)/jni/include/opencv2/highgui

#/Users/vasya/Documents/bloodex/Android/Ogre_Android_SDK/Ogre/include/Components/RTShaderSystem/OgreShaderGenerator.h

LOCAL_CFLAGS += -I$(PATH_OGRE)/include/
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/Build
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/OgreMain
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/PlugIns/OctreeSceneManager
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/PlugIns/ParticleFX
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/Components/Overlay
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/Components/RTShaderSystem
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/RenderSystems/GLES2
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/RenderSystems/GLES2/EGL
LOCAL_CFLAGS += -I$(PATH_OGRE)/include/RenderSystems/GLES2/EGL/Android

LOCAL_CFLAGS += -fexceptions -frtti -x c++ -D___ANDROID___ -DANDROID -DZZIP_OMIT_CONFIG_H 

LOCAL_SRC_FILES := OgreActivityJNI.cpp 

include $(BUILD_SHARED_LIBRARY) 

$(call import-module,android/cpufeatures) 
$(call import-module,android/native_app_glue) 
