#define OGRE_STATIC_GLES2
#define OGRE_STATIC_ParticleFX
#define OGRE_STATIC_OctreeSceneManager
#include <jni.h>
#include <EGL/egl.h>
#include <android/api-level.h>
#include <android/native_window_jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include "OgrePlatform.h"
#include "OgreRoot.h"
#include "OgreRenderWindow.h"
#include "OgreArchiveManager.h"
#include "OgreViewport.h"

#define OGRE_STATIC_ParticleFX
#define OGRE_STATIC_OctreeSceneManager
#include "OgreStaticPluginLoader.h"
#include "Ogre.h"
#include "OgreRenderWindow.h"
#include "OgreStringConverter.h"

#include "Android/OgreAndroidEGLWindow.h"
#include "Android/OgreAPKFileSystemArchive.h"
#include "Android/OgreAPKZipArchive.h"

#include <android/log.h>

#include <android_native_app_glue.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "OgreGLES2Plugin.h"

// #ifdef OGRE_BUILD_PLUGIN_OCTREE
// #	include "OgreOctreePlugin.h"
// #endif

// #ifdef OGRE_BUILD_PLUGIN_PFX
// #	include "OgreParticleFXPlugin.h"
// #endif

// #ifdef OGRE_BUILD_COMPONENT_OVERLAY
// #	include "OgreOverlaySystem.h"
// #endif

// #include "OgreConfigFile.h"

// #ifdef OGRE_BUILD_RENDERSYSTEM_GLES2
// #	include "OgreGLES2Plugin.h"
// #	define GLESRS GLES2Plugin
// #else
// #	include "OgreGLESPlugin.h"
// #	define GLESRS GLESPlugin
// #endif

using namespace Ogre;

static bool gInit = false;


// #ifdef OGRE_BUILD_PLUGIN_OCTREE
// static Ogre::OctreePlugin* gOctreePlugin = NULL;
// #endif

// #ifdef OGRE_BUILD_PLUGIN_PFX
// static Ogre::ParticleFXPlugin* gParticleFXPlugin = NULL;
// #endif

// #ifdef OGRE_BUILD_COMPONENT_OVERLAY
// static Ogre::OverlaySystem* gOverlaySystem = NULL; 
// #endif


#define APPNAME "MyApp"

// static Ogre::GLESRS* gGLESPlugin = NULL;

static Ogre::RenderWindow* gRenderWnd = NULL;
static Ogre::Root* gRoot = NULL;
static Ogre::StaticPluginLoader* gStaticPluginLoader = NULL;
static AAssetManager* gAssetMgr = NULL;
static Ogre::SceneManager* gSceneMgr = NULL;

static Ogre::Camera* camera = NULL;
static Ogre::Entity* pEntity = NULL;

static JavaVM* gVM = NULL;

static float startX = -200.0f;
static float startY = 0.0f;
static float startZ =  0.0f;

// static Ogre::Camera* camera  = NULL;




Ogre::Entity *mEntity;                 // The Entity we are animating
Ogre::SceneNode *mNode;                // The -SceneNode that the Entity is attached to
std::deque<Ogre::Vector3> mWalkList;   // The list of points we are walking to

extern "C" 
{
	JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) 
	{
		gVM = vm;
		return JNI_VERSION_1_4;
	}



	JNIEXPORT void JNICALL 	Java_com_saenko_ogreactivityjni_OgreActivityJNI_create(JNIEnv * env, jobject obj, jobject assetManager)
	{
        if(gInit)
			return;
         
        gRoot = new Ogre::Root();
        #ifdef OGRE_STATIC_LIB
        	gStaticPluginLoader = new Ogre::StaticPluginLoader();
        	gStaticPluginLoader->load();
		#endif
        	gRoot->setRenderSystem(gRoot->getAvailableRenderers().at(0));
        	gRoot->initialise(false);

		// gGLESPlugin = OGRE_NEW GLESRS();
		// gRoot->installPlugin(gGLESPlugin);
			
// #ifdef OGRE_BUILD_PLUGIN_OCTREE
// 		gOctreePlugin = OGRE_NEW OctreePlugin();
// 		gRoot->installPlugin(gOctreePlugin);
// #endif
			
// #ifdef OGRE_BUILD_PLUGIN_PFX
// 		gParticleFXPlugin = OGRE_NEW ParticleFXPlugin();
// 		gRoot->installPlugin(gParticleFXPlugin);
// #endif

// #ifdef OGRE_BUILD_COMPONENT_OVERLAY
// 		gOverlaySystem = OGRE_NEW OverlaySystem(); 
// #endif
		
		// gRoot->setRenderSystem(gRoot->getAvailableRenderers().at(0));
        // gRoot->initialise(false);
        gInit = true;
		
        gAssetMgr = AAssetManager_fromJava(env, assetManager);
		if (gAssetMgr)
		{
			ArchiveManager::getSingleton().addArchiveFactory( new APKFileSystemArchiveFactory(gAssetMgr) );
			ArchiveManager::getSingleton().addArchiveFactory( new APKZipArchiveFactory(gAssetMgr) );
		}
	}
	
	JNIEXPORT void JNICALL Java_com_saenko_ogreactivityjni_OgreActivityJNI_destroy(JNIEnv * env, jobject obj)
	{
		if(!gInit)
			return;
                
        gInit = false;
                
// #ifdef OGRE_BUILD_COMPONENT_OVERLAY
// 		OGRE_DELETE gOverlaySystem; 
// 		gOverlaySystem = NULL;
// #endif

        OGRE_DELETE gRoot;
        gRoot = NULL;
        gRenderWnd = NULL;

// #ifdef OGRE_BUILD_PLUGIN_PFX
// 		OGRE_DELETE gParticleFXPlugin;
// 		gParticleFXPlugin = NULL;
// #endif

// #ifdef OGRE_BUILD_PLUGIN_OCTREE
// 		OGRE_DELETE gOctreePlugin;
// 		gOctreePlugin = NULL;
// #endif

		// OGRE_DELETE gGLESPlugin;
		// gGLESPlugin = NULL;
	}
	
	static Ogre::DataStreamPtr openAPKFile(const Ogre::String& fileName)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "openAPKFile %s",fileName.c_str());
		Ogre::DataStreamPtr stream;
	    AAsset* asset = AAssetManager_open(gAssetMgr, fileName.c_str(), AASSET_MODE_BUFFER);
	    if(asset)
	    {
			off_t length = AAsset_getLength(asset);
	        void* membuf = OGRE_MALLOC(length, Ogre::MEMCATEGORY_GENERAL);
	        memcpy(membuf, AAsset_getBuffer(asset), length);
	        AAsset_close(asset);

	        stream = Ogre::DataStreamPtr(new Ogre::MemoryDataStream(membuf, length, true, true));
	    }
	    return stream;
	}

	static void setupScene()
	{
		__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "setupScene !!!!!!!");
		Ogre::ConfigFile cf;
	    cf.load(openAPKFile("resources.cfg"));
//
		Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
		while (seci.hasMoreElements())
		{
			Ogre::String sec, type, arch;
			sec = seci.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator i;

			for (i = settings->begin(); i != settings->end(); i++)
			{
				type = i->first;
				arch = i->second;
				std::cout << "Register:" << type << "\t" << arch << std::endl;
				__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "type %s arch %s",type.c_str(),arch.c_str());
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation(arch, type, sec);
			}
		}
//
		Ogre::ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup(Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

		gSceneMgr = gRoot->createSceneManager(Ogre::ST_GENERIC);
		//Ogre::Camera*
		camera = gSceneMgr->createCamera("MyCam");

		pEntity = gSceneMgr->createEntity("SinbadInstance", "car.mesh");
		Ogre::SceneNode* pNode = gSceneMgr->getRootSceneNode()->createChildSceneNode();
		pNode->attachObject(pEntity);

		Ogre::Light* pDirLight = gSceneMgr->createLight();
		pDirLight->setDirection(Ogre::Vector3(0,-1,0));
		pDirLight->setType(Ogre::Light::LT_DIRECTIONAL);
		pNode->attachObject(pDirLight);

		camera->setNearClipDistance(1.0f);
		camera->setFarClipDistance(100000.0f);
		camera->setPosition(startX,startY,startZ);
		camera->lookAt(0,0,0);
		camera->setAutoAspectRatio(true);

		Ogre::Viewport* vp = gRenderWnd->addViewport(camera);
		vp->setBackgroundColour(Ogre::ColourValue(0.7,0.5,0.8));


		mEntity = gSceneMgr->createEntity("Robot", "robot.mesh");

			    // Create the scene node
			    mNode = gSceneMgr->getRootSceneNode()->createChildSceneNode("RobotNode", Ogre::Vector3(0.0f, 0.0f, 25.0f));
			    mNode->attachObject(mEntity);
//			    // Create the walking list
//			    mWalkList.push_back(Ogre::Vector3(550.0f,  0.0f,  50.0f ));
//			    mWalkList.push_back(Ogre::Vector3(-100.0f,  0.0f, -200.0f));
//		//	    // Create objects so we can see movement
//			    Ogre::Entity *ent;
//			    Ogre::SceneNode *node;
//		//
//			    ent = gSceneMgr->createEntity("Knot1", "car.mesh");
//			    node = gSceneMgr->getRootSceneNode()->createChildSceneNode("Knot1Node", Ogre::Vector3(0.0f, -10.0f,  25.0f));
//			    node->attachObject(ent);
//			    node->setScale(0.1f, 0.1f, 0.1f);

		__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "setupScene finish !!!!!");
	}

	static void moveCamera()
		{
			while(startX != -1000){
				startX -= 1.0f;
				camera->setPosition(startX,startY,startZ);
				__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "startX=%f",startX);
			}
		}
    JNIEXPORT void JNICALL Java_com_saenko_ogreactivityjni_OgreActivityJNI_initWindow(JNIEnv * env, jobject obj,  jobject surface)
	{
    	__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "initWindow");
		if(surface)
		{
			ANativeWindow* nativeWnd = ANativeWindow_fromSurface(env, surface);
			if (nativeWnd && gRoot)
			{
				if (!gRenderWnd) 
				{
					Ogre::NameValuePairList opt;
					opt["externalWindowHandle"] = Ogre::StringConverter::toString((int)nativeWnd);
					gRenderWnd = Ogre::Root::getSingleton().createRenderWindow("OgreWindow", 0, 0, false, &opt);
					
					
					if(gSceneMgr == NULL)
					{
//						gSceneMgr = gRoot->createSceneManager(Ogre::ST_GENERIC);
//						pCamera = gSceneMgr->createCamera("MyCam");
//
//						Ogre::Viewport* vp = gRenderWnd->addViewport(pCamera);
//						vp->setBackgroundColour(Ogre::ColourValue(1,1,0));
						setupScene();
//						moveCamera();
					}
				}
				else
				{
					static_cast<Ogre::AndroidEGLWindow*>(gRenderWnd)->_createInternalResources(nativeWnd, NULL);
				}                        
			}
		}
	}
	
    JNIEXPORT void JNICALL Java_com_saenko_ogreactivityjni_OgreActivityJNI_termWindow(JNIEnv * env, jobject obj)
	{
    	__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "termWindow");
		if(gRoot && gRenderWnd)
		{
			static_cast<Ogre::AndroidEGLWindow*>(gRenderWnd)->_destroyInternalResources();
		}
	}

	JNIEXPORT void JNICALL Java_com_saenko_ogreactivityjni_OgreActivityJNI_renderOneFrame(JNIEnv * env, jobject obj)
	{
//		__android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "renderOneFrame");
		if(gRenderWnd != NULL && gRenderWnd->isActive())
		{
			try
			{
				if(gVM->AttachCurrentThread(&env, NULL) < 0)
					return;

				gRenderWnd->windowMovedOrResized();
				gRoot->renderOneFrame();

				//gVM->DetachCurrentThread();
			}catch(Ogre::RenderingAPIException ex) {}
		}
	}
};
